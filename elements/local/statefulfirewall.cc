/*
 * statefulipfilter.{cc,hh} -- Stateful IP-packet filter
 *
 */

#include <click/config.h>
#include <click/confparse.hh>
#include "statefulfirewall.hh"

/* Add header files as required*/
CLICK_DECLS

int DEFAULTACTION; // This is global action variable

/// Connection stuff /////////////////////////////////////

bool Connection::operator==(const Connection &other) const
{
    /* Use Click's String class's compare() method to compare String members, and normal == operator to compare integer members*/
    return ((this->sourceip.compare(other.sourceip) == 0) && (this->destip.compare(other.destip) == 0) && (this->sourceport == other.sourceport) && (this->destport == other.destport) && (this->proto == other.proto));
}

int Connection::compare(const Connection other) const
{

    /* Compare Connection attributes and save results to *_cmp variables*/

    int sip_cmp = (sourceip.compare(other.sourceip));
    int dip_cmp = (destip.compare(other.destip));
    int sport_cmp = (sourceport) - (other.sourceport);
    int dport_cmp = (destport) - (other.destport);
    int proto_cmp = (proto) - (other.proto);
    int result;

    /* Build Connection object comparison tree */

    if (sip_cmp < 0) result = -1;
    else if (sip_cmp > 0) result = 1;
    else
    {
        if (dip_cmp < 0) result = -1;
        else if (dip_cmp > 0) result = 1;
        else
        {
            if (sport_cmp < 0) result = -1;
            else if (sport_cmp > 0) result = 1;
            else
            {
                if (dport_cmp < 0) result = -1;
                else if (dport_cmp > 0) result = 1;
                else
                {
                    if (proto_cmp < 0) result = -1;
                    else if (proto_cmp > 0) result = 1;
                    else result = 0;
                }
            }
        }
    }
    return result; // -1 if less, 0 if equal, 1 if more
}

bool Connection::is_forward()
{
    return isfw;
}
/* Reverse connection function for proper canonicalization */
Connection Connection::reverse() const
{
    return Connection(destip, sourceip, destport, sourceport, proto, true);
}


/// StatefulFirewall stuff /////////////////////////////////////

StatefulFirewall::StatefulFirewall(){};
StatefulFirewall::~StatefulFirewall(){};

/* Main function that takes the decision of firewall and sends it to output ports of Click */
void StatefulFirewall::push(int port, Packet *p)
{
    int result = filter_packet(p);

    if (result == 0) output(0).push(p);
    if (result == 1) output(1).push(p);

}

int StatefulFirewall::configure(Vector<String> &conf, ErrorHandler *errh)
{

    String pfile;
    int action = 0;

    /* Click's parser function cp_va_kparse */
    if (cp_va_kparse(conf, this, errh,
                     "POLICYFILE", cpkM, cpFilename, &pfile, //read "policy file": make it mandatory, type Filename, to $pfile
                     "DEFAULT", cpkM, cpInteger, &action, // read "default action", mandatory, save to $action
                     cpEnd) < 0)
        return -1;
    DEFAULTACTION = action;
    read_policy_config(pfile);
}

bool StatefulFirewall::check_if_new_connection(const Packet *p)
{

    Connection candidate = get_canonicalized_connection(p); // Convert packet to connection and canonicalize it
    std::map<Connection, int>::const_iterator itr = StatefulFirewall::Connections.find(candidate); // get iterator from Connections map

    return ((itr == Connections.end()) && (p->tcp_header()->th_flags & TH_SYN)); // check if SYN header is present in TCP header

}

bool StatefulFirewall::check_if_connection_reset(const Packet *p)
{
    return ((p->tcp_header()->th_flags & TH_RST) || (p->tcp_header()->th_flags & TH_FIN)); // check if RST or FIN headers are present in TCP header
}

void StatefulFirewall::add_connection(Connection &c, int action)
{
    Connections.insert(std::pair<Connection,int>(c, action)); // add connection to Connections map
}

void StatefulFirewall::delete_connection(Connection &c)
{
    Connections.erase(c); // delete connection from Connections map
}

Connection StatefulFirewall::get_canonicalized_connection(const Packet *p)
{
    String sourceip;
    String destip;
    int sourceport;
    int destport;
    int proto;
    bool isfw;
    int src_ip;
    int dst_ip;

    src_ip = ntohl(IPAddress(p->ip_header()->ip_src).addr()); // convert String to integer for later use for comparison
    dst_ip = ntohl(IPAddress(p->ip_header()->ip_dst).addr());

    proto = p->ip_header()->ip_p;

    if (src_ip < dst_ip) { // if source < destination then connection is directed forward, set all members as usual
        isfw = true;
        sourceip = inet_ntoa(p->ip_header()->ip_src);
        destip = inet_ntoa(p->ip_header()->ip_dst);
        sourceport = ntohs(p->tcp_header()->th_sport);
        destport = ntohs(p->tcp_header()->th_dport);
    } else {  //if source > or equal to destination then connection is directed backwards, set all members to opposites
        isfw = false;
        sourceip = inet_ntoa(p->ip_header()->ip_dst);
        destip = inet_ntoa(p->ip_header()->ip_src);
        sourceport = ntohs(p->tcp_header()->th_dport);
        destport = ntohs(p->tcp_header()->th_sport);
    }

    return Connection(sourceip, destip, sourceport, destport, proto, isfw);
}

int StatefulFirewall::read_policy_config(String filename)
{
    const int MAX_CHARS_PER_LINE = 60; // maximum number of characters per line
    const int MAX_TOKENS_PER_LINE = 20; // maximum number of tokens per line
    const char* const DELIMITER = " "; // character delimiter

    string line;
    ifstream fil;

    String sourceip;
    String destip;
    int sourceport;
    int destport;
    int proto;
    int action;

    fil.open(filename.c_str()); // open file
    if (!fil.good()) // if file cannot be opened, die and return 1
        return 1;


    char buf[MAX_CHARS_PER_LINE];
    while (fil.getline(buf, MAX_CHARS_PER_LINE))
    {

        int n = 0;
        const char* comment = "#"; // a comment delimiter
        const char* token[MAX_TOKENS_PER_LINE] = {};
        char first = buf[0];

        if (strncmp(comment, &first, 1) != 0) // if first character is not comment...
        {
            token[0] = strtok(buf, DELIMITER); // ...tokenize!
            if (token[0]) {
                for (n = 1; n < MAX_TOKENS_PER_LINE; n++)
                {
                    token[n] = strtok(0, DELIMITER);
                    if (!token[n]) break;
                }
            }
            sourceip = String(token[0]); // set all variables to token values
            destip = String(token[2]);
            sourceport = atoi(token[1]);
            destport = atoi(token[3]);
            proto = atoi(token[4]);
            action = atoi(token[5]);

            Policy rules(sourceip, destip, sourceport, destport, proto, action); //create Policy object from variables

            list_of_policies.push_back(rules); //add Policy to the policies vector
        }
    }
    return 0;
}


int StatefulFirewall::filter_packet(const Packet *p)
{
    int action = DEFAULTACTION; // set default action
    Connection c = get_canonicalized_connection(p); // get canonicalized connection

    if (check_if_new_connection(p)) { // if connection does not exist in database...
        if (c.is_forward()) { // ... and has forward direction
            for (int i = 0; i < list_of_policies.size(); i++) { // iterate through policies
                Connection rule = list_of_policies[i].getConnection(); // get policy
                if (rule == c) { // if policy corresponds to the connection
                    action = list_of_policies[i].getAction(); // set the action
                    break;
                }
            }
            add_connection(c, action); // add connection to map

        } else { // ... and has reverse direction
            Connection reversed = c.reverse(); // reverse it
            for (int i = 0; i < list_of_policies.size(); i++)
            {
                Connection rule = list_of_policies[i].getConnection();
                if (rule == reversed) {
                    action = list_of_policies[i].getAction();
                    break;
                }
            }
            add_connection(reversed, action);
        }
    }
    else if (!check_if_new_connection(p)) // if connection exists in database
    {
        std::map<Connection, int>::const_iterator itr = Connections.find(c); // get the iterator
        action = 0;

        if (itr != Connections.end()) //if corresponding connection is found
            action = itr->second; // set action the one corresponding to connection

    }
    else if (check_if_connection_reset(p)) //if TCP reset flag is set
    {
        std::map<Connection, int>::const_iterator itr = Connections.find(c); // get iterator
        action = 0;
        if (itr != Connections.end()){  //if reset is sent for the connection that exists in DB
            action = 1; //allow the packet
            delete_connection(c); // delete connection from database
        }
    }
    return action;

}
/// Policy stuff /////////////////////////////////////


int Policy::getAction()
{
    return action;
}

CLICK_ENDDECLS
        EXPORT_ELEMENT(StatefulFirewall)
